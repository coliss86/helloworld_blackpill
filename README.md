# Helloworld Pad

A single switch macropad to start learning handwired a keyboard with a STM32F401, aka *Blackpill*, microcontroller .

![Wiring](doc/wiring.jpg)

## Electronic

The switch is connected to `B0` and `A7` pin of the STM32.

Here is the pinout of the Blackpill board:

![blackpill pinout](https://raw.githubusercontent.com/WeActStudio/WeActStudio.MiniSTM32F4x1/master/images/STM32F4x1_PinoutDiagram_RichardBalint.png)

Source: https://github.com/WeActStudio/WeActStudio.MiniSTM32F4x1

## Flash Instructions

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).

### Import QMK firmware

```
git clone https://gitlab.com/coliss86/helloworld_blackpill
git clone -b 0.24.8 https://github.com/qmk/qmk_firmware
cd qmk_firmware/keyboards
ln -s ../../helloworld_blackpill
```

### Flash the firmware

- First, put the microcontroller in *dfu* mode, by pressing *BOOT0*, then *NRST*, release it and release *BOOT0*
- then compile the firmware:

```
qmk compile -kb helloworld_blackpill -km default
```

- next, flash the keyboard:

```
qmk flash -kb helloworld_blackpill -km default
```

## Update qmk

To update qmk, go to the `qmk_firmware` and run the following commands:
```
cd qmk_firmware
git fetch -a
git checkout <new_tag>
make git-submodule
```
